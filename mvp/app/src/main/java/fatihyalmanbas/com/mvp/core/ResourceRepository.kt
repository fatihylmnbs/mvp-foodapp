package fatihyalmanbas.com.mvp.core

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.annotation.DrawableRes
import android.support.annotation.IntegerRes
import android.support.annotation.StringRes
import android.support.v7.content.res.AppCompatResources

object ResourceRepository {

	private val context: Context by lazy { Application.Instance.applicationContext }

	fun getString(@StringRes id: Int): String = context.getString(id)

	fun getInteger(@IntegerRes id: Int): Int = context.resources.getInteger(id)

	fun getDrawable(@DrawableRes id: Int): Drawable = AppCompatResources.getDrawable(context, id)!!

}