package fatihyalmanbas.com.mvp.login.onboarding

import fatihyalmanbas.com.mvplibrary.core.BasePresenter
import fatihyalmanbas.com.mvplibrary.core.BaseView

interface OnboardingContract {

	interface View : BaseView<View, Presenter>

	abstract class Presenter(view: View) : BasePresenter<View>(view)

}
