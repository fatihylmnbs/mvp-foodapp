package fatihyalmanbas.com.mvp.core

import fatihyalmanbas.com.mvplibrary.callbacks.ActivityLayoutLifecycleCallbacks
import fatihyalmanbas.com.mvplibrary.callbacks.PresentableLifecycleCallbacks
import android.app.Application as AndroidApplication

class Application : AndroidApplication() {

	init {
		Application.Instance = this
	}

	override fun onCreate() {
		super.onCreate()
		registerActivityLifecycleCallbacks(ActivityLayoutLifecycleCallbacks())
		registerActivityLifecycleCallbacks(PresentableLifecycleCallbacks())
	}

	companion object {
		lateinit var Instance: Application
	}
}