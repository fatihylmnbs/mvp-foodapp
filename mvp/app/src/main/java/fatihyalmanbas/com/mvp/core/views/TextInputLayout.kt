package fatihyalmanbas.com.mvp.core.views

import android.content.Context
import android.util.AttributeSet
import android.support.design.widget.TextInputLayout as AndroidTextInputLayout

class TextInputLayout @JvmOverloads constructor(
		context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AndroidTextInputLayout(context, attrs, defStyleAttr) {

}