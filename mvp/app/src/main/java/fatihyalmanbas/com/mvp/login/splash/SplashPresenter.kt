package fatihyalmanbas.com.mvp.login.splash

import fatihyalmanbas.com.mvp.login.login.LoginActivity
import io.reactivex.Completable
import java.util.concurrent.TimeUnit

class SplashPresenter(view: SplashContract.View) : SplashContract.Presenter(view) {

	private val dataSource: SplashDataSource by lazy { SplashDataSourceImpl }

	override fun subscribe() {
		if (!dataSource.hasLastOpenDate()) {
			// Launching for the first time
			Completable
					.complete()
					.delay(500L, TimeUnit.MILLISECONDS)
					.subscribe {
						dataSource.setLastOpenDate(System.currentTimeMillis())
						navigateToLogin() ?: dataSource.setLastOpenDate(0)
					}.disposeOnUnsubscribe()
		} else {
			navigateToLogin()
		}
	}

	private fun navigateToLogin() = doIfViewAttached {
		navigationHandler.handleNavigation(LoginActivity.getNavigationBundle())
	}

}