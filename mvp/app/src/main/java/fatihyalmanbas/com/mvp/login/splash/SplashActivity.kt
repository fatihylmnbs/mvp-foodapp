package fatihyalmanbas.com.mvp.login.splash

import fatihyalmanbas.com.mvp.R
import fatihyalmanbas.com.mvp.core.BaseActivity
import fatihyalmanbas.com.mvplibrary.callbacks.ActivityLayout

class SplashActivity : BaseActivity<SplashContract.View, SplashContract.Presenter>(),
		SplashContract.View,
		ActivityLayout {

	override val presenter: SplashContract.Presenter by lazy { SplashPresenter(this) }

	override val layoutId: Int = R.layout.activity_splash
}
