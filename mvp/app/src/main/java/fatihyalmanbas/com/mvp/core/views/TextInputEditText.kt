package fatihyalmanbas.com.mvp.core.views

import android.content.Context
import android.util.AttributeSet
import android.widget.EditText as AndroidEditText

class EditText : AndroidEditText {

	constructor(context: Context) : super(context)

	constructor(context: Context,
	            attrs: AttributeSet
	) : super(context, attrs)

	constructor(context: Context,
	            attrs: AttributeSet,
	            defStyleAttr: Int
	) : super(context, attrs, defStyleAttr)
}