package fatihyalmanbas.com.mvp.core.views

import android.content.Context
import android.util.AttributeSet
import android.support.v7.widget.AppCompatImageView as AndroidImageView

class ImageView @JvmOverloads constructor(
		context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AndroidImageView(context, attrs, defStyleAttr) {
}