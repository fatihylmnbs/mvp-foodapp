package fatihyalmanbas.com.mvp.core

import android.support.v4.app.Fragment
import fatihyalmanbas.com.mvplibrary.core.BasePresenter
import fatihyalmanbas.com.mvplibrary.core.BaseView
import fatihyalmanbas.com.mvplibrary.navigation.FragmentNavigationHandlerImpl
import fatihyalmanbas.com.mvplibrary.navigation.NavigationOwner

abstract class BaseFragment<
		View : BaseView<View, Presenter>,
		Presenter : BasePresenter<View>> :
		Fragment(), BaseView<View, Presenter>
{

	override val navigationHandler: NavigationOwner.INavigationHandler by lazy {
		FragmentNavigationHandlerImpl(this)
	}

}