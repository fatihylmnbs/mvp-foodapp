package fatihyalmanbas.com.mvp.core

import android.support.v7.app.AppCompatActivity
import fatihyalmanbas.com.mvplibrary.core.BasePresenter
import fatihyalmanbas.com.mvplibrary.core.BaseView
import fatihyalmanbas.com.mvplibrary.navigation.ActivityNavigationHandlerImpl
import fatihyalmanbas.com.mvplibrary.navigation.NavigationOwner

abstract class BaseActivity<
		View : BaseView<View, Presenter>,
		Presenter : BasePresenter<View>> :
		AppCompatActivity(), BaseView<View, Presenter> {

	override val navigationHandler: NavigationOwner.INavigationHandler by lazy {
		ActivityNavigationHandlerImpl(this)
	}

}