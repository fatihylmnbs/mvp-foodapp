package fatihyalmanbas.com.mvp.login.splash

import android.annotation.SuppressLint
import android.content.Context
import fatihyalmanbas.com.mvp.core.Application

@SuppressLint("StaticFieldLeak")
object SplashDataSourceImpl : SplashDataSource {
	private const val INSTANCE_NAME_SHARED_PREF_SPLASH = "SplashData"

	private const val KEY_LAST_OPEN_DATE = "key_last_open_date"
	private const val DEFAULT_LAST_OPEN_MILLIS = 0L

	private var context: Context = Application.Instance.applicationContext

	private val sharedPreferences = context.getSharedPreferences(
			INSTANCE_NAME_SHARED_PREF_SPLASH,
			Context.MODE_PRIVATE
	)

	override fun getLastOpenDate(): Long {
		return sharedPreferences.getLong(KEY_LAST_OPEN_DATE, DEFAULT_LAST_OPEN_MILLIS)
	}

	override fun setLastOpenDate(time: Long) {
		sharedPreferences.edit().putLong(KEY_LAST_OPEN_DATE, time).apply()
	}

	override fun hasLastOpenDate(): Boolean = sharedPreferences.contains(KEY_LAST_OPEN_DATE)

}