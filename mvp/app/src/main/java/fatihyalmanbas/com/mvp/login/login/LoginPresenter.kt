package fatihyalmanbas.com.mvp.login.login

import fatihyalmanbas.com.mvp.login.onboarding.OnboardingActivity

class LoginPresenter(view: LoginContract.View) : LoginContract.Presenter(view) {

	override fun onLoginClick() {
		doIfViewAttached {
			navigationHandler.handleNavigation(OnboardingActivity.getNavigationBundle())
		}
	}

}