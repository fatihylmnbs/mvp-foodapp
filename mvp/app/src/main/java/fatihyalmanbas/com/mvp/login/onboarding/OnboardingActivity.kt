package fatihyalmanbas.com.mvp.login.onboarding

import fatihyalmanbas.com.mvp.R
import fatihyalmanbas.com.mvp.core.BaseActivity
import fatihyalmanbas.com.mvplibrary.callbacks.ActivityLayout
import fatihyalmanbas.com.mvplibrary.navigation.ActivityNavigationBundle
import fatihyalmanbas.com.mvplibrary.navigation.NavigationBundle

class OnboardingActivity : BaseActivity<OnboardingContract.View, OnboardingContract.Presenter>(),
		OnboardingContract.View,
		ActivityLayout {

	override val presenter: OnboardingContract.Presenter by lazy { OnboardingPresenter(this) }

	override val layoutId: Int = R.layout.activity_onboarding

	companion object {
		fun getNavigationBundle(): NavigationBundle = ActivityNavigationBundle(
				OnboardingActivity::class.java
		)
	}
}
