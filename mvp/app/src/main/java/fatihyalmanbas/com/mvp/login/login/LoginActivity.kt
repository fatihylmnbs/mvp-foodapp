package fatihyalmanbas.com.mvp.login.login

import android.os.Bundle
import android.os.PersistableBundle
import fatihyalmanbas.com.mvp.R
import fatihyalmanbas.com.mvp.core.BaseActivity
import fatihyalmanbas.com.mvplibrary.callbacks.ActivityLayout
import fatihyalmanbas.com.mvplibrary.navigation.ActivityNavigationBundle
import fatihyalmanbas.com.mvplibrary.navigation.NavigationBundle
import fatihyalmanbas.com.mvplibrary.validator.EmailValidationRule
import fatihyalmanbas.com.mvplibrary.validator.LengthValidationRule
import fatihyalmanbas.com.mvplibrary.validator.ValidationController
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity<LoginContract.View, LoginContract.Presenter>(),
		LoginContract.View,
		ActivityLayout {

	override val layoutId: Int = R.layout.activity_login

	override val presenter: LoginContract.Presenter by lazy { LoginPresenter(this) }

	private val validationController by lazy {
		ValidationController {
			addRule(
					LengthValidationRule(editTextPassword, 4, 21, "Length shall be between 4 to 21")
			)
			addRule(
					EmailValidationRule(editTextEmail, "Email format is not valid")
			)
		}

	}

	override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
		super.onCreate(savedInstanceState, persistentState)
		buttonLogin.setOnClickListener {
			if (validationController.isValid()) {
				presenter.onLoginClick()
			}
		}
	}

	companion object {
		fun getNavigationBundle(): NavigationBundle = ActivityNavigationBundle(
				LoginActivity::class.java
		)
	}
}

