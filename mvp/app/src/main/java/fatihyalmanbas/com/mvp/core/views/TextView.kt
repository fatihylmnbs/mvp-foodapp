package fatihyalmanbas.com.mvp.core.views

import android.content.Context
import android.util.AttributeSet
import android.widget.TextView as AndroidTextView

class TextView @JvmOverloads constructor(
		context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AndroidTextView(context, attrs, defStyleAttr)