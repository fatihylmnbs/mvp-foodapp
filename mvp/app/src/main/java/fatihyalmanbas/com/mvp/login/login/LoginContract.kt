package fatihyalmanbas.com.mvp.login.login

import fatihyalmanbas.com.mvplibrary.core.BasePresenter
import fatihyalmanbas.com.mvplibrary.core.BaseView

interface LoginContract {

	interface View : BaseView<View, Presenter>

	abstract class Presenter(
			view: View
	) : BasePresenter<View>(view) {
		abstract fun onLoginClick()
	}
}