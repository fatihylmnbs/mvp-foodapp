package fatihyalmanbas.com.mvp.login.splash

import fatihyalmanbas.com.mvplibrary.core.BasePresenter
import fatihyalmanbas.com.mvplibrary.core.BaseView

interface SplashContract {

	interface View : BaseView<View, Presenter>

	abstract class Presenter(view: View) : BasePresenter<View>(view)
}