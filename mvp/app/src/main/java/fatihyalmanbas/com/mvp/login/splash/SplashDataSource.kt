package fatihyalmanbas.com.mvp.login.splash

interface SplashDataSource {
	fun getLastOpenDate(): Long

	fun hasLastOpenDate(): Boolean

	fun setLastOpenDate(time: Long)
}