package fatihyalmanbas.com.mvplibrary.navigation

interface AndroidNavigationHandler : NavigationOwner.INavigationHandler {

	override fun handleNavigation(navigationBundle: NavigationBundle) {
		when (navigationBundle) {
			is ActivityNavigationBundle -> navigateToActivity(navigationBundle)
			is FragmentNavigationBundle -> navigateToFragment(navigationBundle)
			else -> navigateToView(navigationBundle)
		}
	}

	fun navigateToActivity(navigationBundle: ActivityNavigationBundle)

	fun navigateToFragment(navigationBundle: FragmentNavigationBundle)

	fun navigateToView(navigationBundle: NavigationBundle) {
		throw UnsupportedOperationException("This navigation bundle is not supported.")
	}

}