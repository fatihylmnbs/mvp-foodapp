package fatihyalmanbas.com.mvplibrary.validator

import io.reactivex.subjects.PublishSubject

class ValidationController(init: ValidationController.() -> ValidationController = { this }) {

	private val validationRuleList: MutableList<ValidationRule> = mutableListOf()

	private val validationChangeSubject = PublishSubject.create<Boolean>()

	fun addRule(validationRule: ValidationRule): ValidationController {
		validationRuleList += validationRule
		return this
	}

	fun removeRule(rule: ValidationRule): ValidationController {
		validationRuleList -= rule
		return this
	}

	fun isValid(showError: Boolean = true): Boolean {
		validationRuleList.forEach { rule ->
			if (rule.isValid(showError).not()) {
				return false
			}
		}
		return true
	}

}
