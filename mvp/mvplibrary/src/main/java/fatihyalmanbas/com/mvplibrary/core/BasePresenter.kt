package fatihyalmanbas.com.mvplibrary.core

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.lang.ref.WeakReference

abstract class BasePresenter<
		View : BaseView<View, *>>(
		view: View
) {

	private val disposeBag = CompositeDisposable()

	private var view: WeakReference<View> = WeakReference(view)

	/**
	 * Subscribes to view events.
	 */
	open fun subscribe() {

	}

	/**
	 * Unsubscribe from view events.
	 */
	open fun unsubscribe() {
		disposeBag.clear()
	}

	@Suppress("RedundantUnitExpression")
	protected fun doIfViewAttached(action: View.() -> Unit): Unit? {
		val viewReference = view.get() ?: return null
		viewReference.action()
		return Unit
	}

	protected fun Disposable.disposeOnUnsubscribe() {
		disposeBag.add(this)
	}
}
