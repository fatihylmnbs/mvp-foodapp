package fatihyalmanbas.com.mvplibrary.validator

import android.view.View

interface ValidationRule {

	val view: View

	var isRequired: Boolean

	var errorText : CharSequence

	fun isValid(showError: Boolean): Boolean

}