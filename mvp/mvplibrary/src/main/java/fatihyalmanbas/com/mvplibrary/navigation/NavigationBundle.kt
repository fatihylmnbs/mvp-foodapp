package fatihyalmanbas.com.mvplibrary.navigation

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.annotation.IdRes
import android.support.annotation.IntDef
import android.support.v4.app.Fragment
import android.view.View

sealed class NavigationBundle

class ActivityNavigationBundle(
		private val activityClass: Class<out Activity>,
		private val bundle: Bundle = Bundle.EMPTY
) : NavigationBundle() {
	fun getIntent(context: Context) = Intent(context, activityClass).apply {
		putExtras(bundle)
	}
}

class FragmentNavigationBundle(
		val fragment: Fragment,
		val fragmentTag: String? = null,
		@TransactionType val transactionType: Int = TRANSACTION_REPLACE,
		@IdRes val fragmentContainerId: Int = View.NO_ID
) : NavigationBundle() {

	fun hasContainerId(): Boolean = fragmentContainerId != View.NO_ID
}

@Retention(AnnotationRetention.SOURCE)
@IntDef(TRANSACTION_ADD,
		TRANSACTION_REPLACE,
		TRANSACTION_REMOVE)
annotation class TransactionType

const val TRANSACTION_ADD = 0
const val TRANSACTION_REPLACE = 1
const val TRANSACTION_REMOVE = 2