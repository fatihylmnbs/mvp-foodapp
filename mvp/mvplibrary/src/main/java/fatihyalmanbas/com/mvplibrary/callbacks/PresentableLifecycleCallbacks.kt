package fatihyalmanbas.com.mvplibrary.callbacks

import android.app.Activity
import fatihyalmanbas.com.mvplibrary.core.BaseView

class PresentableLifecycleCallbacks : SimpleActivityCallback {
	override fun onActivityResumed(activity: Activity) {
		super.onActivityResumed(activity)
		if (activity is BaseView<*, *>) {
			activity.presenter.subscribe()
		}
	}

	override fun onActivityPaused(activity: Activity) {
		if (activity is BaseView<*, *>) {
			activity.presenter.unsubscribe()
		}
		super.onActivityPaused(activity)
	}
}