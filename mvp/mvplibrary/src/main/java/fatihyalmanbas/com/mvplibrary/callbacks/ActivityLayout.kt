package fatihyalmanbas.com.mvplibrary.callbacks

interface ActivityLayout {

	/**
	 * Layout Id for this activity
	 */
	val layoutId: Int

}