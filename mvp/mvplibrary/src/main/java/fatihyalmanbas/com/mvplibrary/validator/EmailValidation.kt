package fatihyalmanbas.com.mvplibrary.validator

import android.widget.EditText
import java.util.regex.Pattern

class EmailValidationRule(override val view: EditText,
                          override var errorText: CharSequence
) : ValidationRule {

	private val emailRegex = Pattern.compile(REGEX_EMAIL)

	override var isRequired: Boolean = true

	override fun isValid(showError: Boolean): Boolean {
		if (view.hasFocus() || emailRegex.matcher(view.text).matches()) {
			view.error = ""
			return true
		}

		if (showError) {
			view.error = errorText
		}
		return false
	}

	companion object {
		private const val REGEX_EMAIL = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]|[\\w-]{2,}))@((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9]))|([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
	}
}