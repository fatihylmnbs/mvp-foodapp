package fatihyalmanbas.com.mvplibrary.callbacks

import android.app.Activity
import android.os.Bundle

class ActivityLayoutLifecycleCallbacks : SimpleActivityCallback {

	override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
		if (activity is ActivityLayout) {
			activity.setContentView(activity.layoutId)
		}
	}
}