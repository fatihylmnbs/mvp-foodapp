package fatihyalmanbas.com.mvplibrary.validator

import android.widget.TextView

class LengthValidationRule(override val view: TextView,
                           val minLegth: Int,
                           val maxLength: Int,
                           override var errorText: CharSequence
) : ValidationRule {

	override var isRequired = true

	override fun isValid(showError: Boolean): Boolean {
		if (view.hasFocus() || (view.text.length in minLegth..maxLength)) {
			view.error = ""
			return true
		}

		if (showError) {
			view.error = errorText
		}
		return false
	}
}