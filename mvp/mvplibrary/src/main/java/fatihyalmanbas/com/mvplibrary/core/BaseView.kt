package fatihyalmanbas.com.mvplibrary.core

import fatihyalmanbas.com.mvplibrary.navigation.NavigationOwner

interface BaseView<Self : BaseView<Self, Presenter>, Presenter : BasePresenter<Self>>
	: NavigationOwner {

	val presenter: Presenter

}