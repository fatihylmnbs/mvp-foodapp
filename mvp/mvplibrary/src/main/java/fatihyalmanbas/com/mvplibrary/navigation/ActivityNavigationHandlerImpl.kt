package fatihyalmanbas.com.mvplibrary.navigation

import android.support.annotation.IdRes
import android.support.v7.app.AppCompatActivity
import fatihyalmanbas.com.mvplibrary.core.FragmentOwner
import java.lang.ref.WeakReference

class ActivityNavigationHandlerImpl(activity: AppCompatActivity) : AndroidNavigationHandler {

	private val activityReference: WeakReference<AppCompatActivity> = WeakReference(activity)

	override fun navigateToActivity(navigationBundle: ActivityNavigationBundle) {
		val activity = activityReference.get() ?: return
		activity.startActivity(navigationBundle.getIntent(activity))
	}

	override fun navigateToFragment(navigationBundle: FragmentNavigationBundle) {
		val activity = activityReference.get() ?: return
		@IdRes val containerId: Int =
				if (navigationBundle.hasContainerId()) navigationBundle.fragmentContainerId
				else (activity as? FragmentOwner)?.fragmentContainerId ?: return

		val transaction = activity.supportFragmentManager.beginTransaction()
		with(navigationBundle) {
			when (transactionType) {
				TRANSACTION_ADD -> transaction.add(containerId, fragment, fragmentTag)
				TRANSACTION_REPLACE -> transaction.replace(containerId, fragment, fragmentTag)
				TRANSACTION_REMOVE -> transaction.remove(fragment)
				else -> return
			}
		}

		transaction.commit()
	}
}