package fatihyalmanbas.com.mvplibrary.navigation

import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import fatihyalmanbas.com.mvplibrary.core.FragmentOwner
import java.lang.ref.WeakReference

class FragmentNavigationHandlerImpl(fragment: Fragment) : AndroidNavigationHandler {

	private val fragmentReference: WeakReference<Fragment> = WeakReference(fragment)

	override fun navigateToActivity(navigationBundle: ActivityNavigationBundle) {
		val fragment = fragmentReference.get() ?: return
		val navigationOwner = fragment.activity as NavigationOwner

		navigationOwner.navigationHandler.handleNavigation(navigationBundle)
	}

	override fun navigateToFragment(navigationBundle: FragmentNavigationBundle) {
		val fragment = fragmentReference.get() ?: return
		@IdRes val containerId: Int =
				if (navigationBundle.hasContainerId()) navigationBundle.fragmentContainerId
				else (fragment as? FragmentOwner)?.fragmentContainerId ?: return

		val transaction = fragment.childFragmentManager.beginTransaction()
		with(navigationBundle) {
			when (transactionType) {
				TRANSACTION_ADD -> transaction.add(containerId, fragment, fragmentTag)
				TRANSACTION_REPLACE -> transaction.replace(containerId, fragment, fragmentTag)
				TRANSACTION_REMOVE -> transaction.remove(fragment)
				else -> return
			}
		}
		transaction.commit()
	}
}