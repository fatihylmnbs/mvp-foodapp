package fatihyalmanbas.com.mvplibrary.core

/**
 * A view that manipulates multiple fragments one at a time.
 */
interface FragmentOwner {

	/**
	 * Convenient frame layout id to replace fragments
	 */
	val fragmentContainerId: Int

}
