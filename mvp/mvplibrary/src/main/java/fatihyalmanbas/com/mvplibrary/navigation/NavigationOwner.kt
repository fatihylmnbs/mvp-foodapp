package fatihyalmanbas.com.mvplibrary.navigation

/**
 * A view that can handle
 */
interface NavigationOwner {

	val navigationHandler: INavigationHandler
		get() = NoOpNavigationHandler

	/**
	 * An interface that handles upcoming navigation bundles.
	 */
	interface INavigationHandler {

		fun handleNavigation(navigationBundle: NavigationBundle)
	}

	private object NoOpNavigationHandler : INavigationHandler {
		override fun handleNavigation(navigationBundle: NavigationBundle) {
			// Do nothing
		}
	}
}